﻿using System;
using System.Collections.Generic;

namespace DaySix
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 3) 
                { 
                    throw new Exception("This program takes two inputs the first denoting which algorithm to use, the  number of days to calculate the population for and the second the file path of a file containing a list of inputs"); 
                }
                int algorithm = int.Parse(args[0]); 
                int numberOfDays = int.Parse(args[1]);

                string fileInput = System.IO.File.ReadAllText(args[2]);

                var inputs = fileInput.Split(",");

                List<int> population = new List<int>(); 
                for (int i = 0; i < inputs.Length; i++) 
                {
                    population.Add(int.Parse(inputs[i])); 
                }

                DateTime start = DateTime.Now; 
                long result = -1; 
                if (algorithm == 0) 
                { 
                    result = Logic.CalculatePopulation(population, numberOfDays); 
                } 
                else 
                {
                    result = Logic.EfficientCalculatePopulation(population, numberOfDays);
                }

                DateTime end = DateTime.Now; 

                Console.WriteLine(string.Format("The population of laternfish after {0} days is {1}.", numberOfDays, result)); 
                Console.WriteLine(end.Subtract(start).ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unexpected error"); 
                Console.WriteLine(ex.Message);
            }
        }
    }

    public static class Logic
    {
        public static int CalculatePopulation(List<int> population, int days)
        {
            for (int day = 0; day < days; day++)
            {
                int count = population.Count;

                for (int i = 0; i < count; i++)
                {
                    if (population[i] == 0)
                    {
                        population[i] = 6;
                        population.Add(8);
                    }
                    else
                    {
                        population[i]--;
                    }
                }
            }

            return population.Count;
        }

        public static long EfficientCalculatePopulation(List<int> initialPopulation, int days)
        {
            long populationCount = initialPopulation.Count;

            List<long> population = new List<long>() { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < initialPopulation.Count; i++)
            {
                population[initialPopulation[i]]++;
            }

            for (int day = 0; day < days; day++)
            {
                long dailyPopulation = population[0];
                populationCount += dailyPopulation;
                population[7] += dailyPopulation;
                population.RemoveAt(0);
                population.Add(dailyPopulation);
            }

            return populationCount;
        }
    }
}
