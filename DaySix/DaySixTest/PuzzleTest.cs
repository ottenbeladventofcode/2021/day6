using DaySix;
using System;
using System.Collections.Generic;
using Xunit;

namespace DaySixTest
{
    public class PuzzleTest
    {
        [Fact]
        public void PuzzleOneTest()
        {
            List<int> initialPopulation = new List<int>() { 3, 4, 3, 1, 2 };
            int expectedResult = 5934;

            int result = Logic.CalculatePopulation(initialPopulation, 80);

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void PuzzleOneTestEfficient()
        {
            List<int> initialPopulation = new List<int>() { 3, 4, 3, 1, 2 };
            long expectedResult = 5934;

            long result = Logic.EfficientCalculatePopulation(initialPopulation, 80);

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void PuzzleTwoTest()
        {
            List<int> initialPopulation = new List<int>() { 3, 4, 3, 1, 2 };
            long expectedResult = 26984457539;

            long result = Logic.EfficientCalculatePopulation(initialPopulation, 256);

            Assert.Equal(expectedResult, result);
        }
    }
}
